function changeColor(){
    var listTR = document.getElementsByTagName("TR");

    for (let index = 0; index < listTR.length; index++) {
        listTR[index].style.background = "red";
    }
}

function changeColor2(){
    //CSS styles must be removed before adding new ones
    var listTR = document.getElementsByTagName("TR");
    
    for (let index = 0; index < listTR.length; index++) {
        listTR[index].style.background= "";
    }
    
    //This way, elements with "table" class are retreived
    //from the element with ID divTable
    listTR = document.getElementById("divTable")
    .getElementsByClassName("table");

    for (let index = 0; index < listTR.length; index++) {
        listTR[index].style.background= "yellow";
    }
}
window.onload = function(){
    var num, elem;
    do{
        num = prompt("How many elements would you like to generate?");
    }while(num.toString().trim()=="" || this.isNaN(num) || num<=0);

    do{
        elem = prompt("Which element would you like to generate?");
    }while(elem.toString().trim()=="");

    this.switchElem(num, elem);
};

function switchElem(num, elem){
    var obj, opt;

    elem = elem.toUpperCase();

    for (let index = 0; index < num; index++) {
        switch (elem) {
            case "BUTTON":
                obj = document.createElement(elem);
                obj.appendChild(document.createTextNode("Hola"));      
                obj.setAttribute("class", "btn btn-success");  
                obj.onclick = function(){objOnClick()};      
                document.body.appendChild(obj);                 
                break;
            case "DIV":
                obj = document.createElement(elem);
                obj.appendChild(document.createTextNode("Hola"));   
                obj.setAttribute("id", "div"+index)
                document.body.appendChild(obj); 
                break;
            case "SELECT":
                obj = document.createElement(elem);
                opt = document.createElement("option");
                opt.value = "blonde";
                opt.text = "Blonde";
                opt.setAttribute("selected", "selected");
                obj.appendChild(opt);
                document.body.appendChild(obj);

                break;
            default:
                alert("The component is not allowed");
                break;
        }
        
    }
}

function objOnClick(){
    window.print();
}
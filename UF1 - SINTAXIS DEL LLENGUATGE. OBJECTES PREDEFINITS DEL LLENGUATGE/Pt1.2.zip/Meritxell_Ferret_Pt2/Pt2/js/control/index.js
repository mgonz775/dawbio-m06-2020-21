/**
 * @file Manages the client-side code for index.html file.
 * @author Meritxell Ferret 
 * @version 1.0
 * @date 30/10/2020
 */

/**
 * Window onload function. It call to another function.
 * @listens window.onload event
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
window.onload = function(){
    this.clickCancel();   
};

/**
 * introduceToDataBase function. Call to create an input table and displays the divs.
 * @listens button.click event called introduceToDataBase
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function introduceToDataBase() {
    //iframe
    var myIframe = document.getElementById("myIframe");

    //collect var from iframe
    var typeMolecule = myIframe.contentWindow.document.getElementById("sel1").value;
    var numElem = myIframe.contentWindow.document.getElementById("num").value;

    //headerCard.parentNode.removeChild(headerCard); //TODO
    document.getElementById("card2header").innerText = "";
    var headerCard = document.createTextNode("Enter products for category "+typeMolecule+ " code");
    document.getElementById("card2header").appendChild(headerCard);
    this.dropElement("myTable");
    this.createTable (numElem);

    //to display
    document.getElementById("formInput").style.display="none";
    document.getElementById("input2").style.display="block";


}
/**
 * dropElement function. Deletes elements child from a given parent.
 * @listens none
 * @param {string} - id of the element to delete
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function dropElement(id){
	elem = document.getElementById(id);	
	if (elem){
        parent = elem.parentNode;
		parent.removeChild(elem);
	}
}



/**
 * clickCancel function. Go to initial page and reset the introduced values.
 * @listens button.click event called clickCancel to return to intro page. 
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function clickCancel() {
    var myIframe = document.getElementById("myIframe");

    document.getElementById("formInput").style.display="block";
    document.getElementById("input2").style.display="none";

    //reset
    myIframe.contentWindow.document.getElementById("num").value="";
    myIframe.contentWindow.document.getElementById("message").innerText="";

};

/**
 * createTable function. Creates a table element.
 * @listens button.click event called clickCancel to return to intro page. 
 * @param {number} - the number of products to introduce
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function createTable (num) { 
    //create the table structure
    var table = document.createElement("table");
    table.setAttribute("id", "myTable");
    document.getElementById("dynamicTable").appendChild(table);

    //title 
    var title = document.createElement("tr");
    title.setAttribute("id", "title");
    document.getElementById("myTable").appendChild(title);

    var th1 = document.createElement("th");
    var txt1 = document.createTextNode("Producte name");
    th1.appendChild(txt1);
    document.getElementById("title").appendChild(th1);

    var th2 = document.createElement("th");
    var txt2 = document.createTextNode("Code");
    th2.appendChild(txt2);
    document.getElementById("title").appendChild(th2);

    var th3 = document.createElement("th");
    var txt3 = document.createTextNode("Tested");
    th3.appendChild(txt3);
    document.getElementById("title").appendChild(th3);


    //add as many rows as the num enetered by user
    for (let index = 0; index < num; index++) {
        var row = document.createElement("tr");
        row.setAttribute("id", "row"+index);
        document.getElementById("myTable").appendChild(row);

        var td1 = document.createElement("td");
        var input = document.createElement("input");
        input.setAttribute('type', 'text');
        input.setAttribute("id", "inputName"+index);
        td1.appendChild(input);
        document.getElementById("row"+index).appendChild(td1);

        var td2 = document.createElement("td");
        var input = document.createElement("input");
        input.setAttribute('type', 'text');
        input.setAttribute("id", "inputCode"+index);
        input.setAttribute("size", "50");
        td2.appendChild(input);
        document.getElementById("row"+index).appendChild(td2);

        var td3 = document.createElement("td");
        var input = document.createElement("input");
        input.setAttribute('type', 'checkbox');
        input.setAttribute("id", "inputCheckbox"+index);
        td3.appendChild(input);
        document.getElementById("row"+index).appendChild(td3);
    }
  
};

/**
 * toDataBase function. Validates inputs and open a popUpWindow
 * @listens button.click event called toDataBase. 
 * @param none
 * @returns none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function toDataBase() {
    //validate
    valid = true;
    var myIframe = document.getElementById("myIframe");
    var typeMolecule = myIframe.contentWindow.document.getElementById("sel1").value;

    var numElem = myIframe.contentWindow.document.getElementById("num").value;
    for (let index = 0; index < numElem; index++) {
        if(document.getElementById("inputName"+index).value.toString().trim() =="" || 
        document.getElementById("inputCode"+index).value.toString().trim() ==""){
            document.getElementById("messageError").innerText = "All fields required!";
            valid = false;
            break;
        }
        
    } if (valid) {
        for (let index = 0; index < numElem; index++) {
            var inputcode = document.getElementById("inputCode"+index).value.toString().trim();
            if (typeMolecule == "DNA"){
                $validSeq = checkValidSequence (inputcode, DNACHARS);
            } else {
                $validSeq = checkValidSequence (inputcode, AA);
            }

            if($validSeq){
                if(numElem-1 == index) {
                    var decision = confirm("Do you really want to introduce this products?");
                    if(decision) {
                        document.getElementById("messageError").innerText = "";
                        myPopUp = window.open("./popUpWindows/popUpWindow.html", "_blank","width=400px, height=400px");
                    }
                }
            } else {
                document.getElementById("messageError").innerText = "Invalid sequence!";
                break;
            }
        } 
    }
}


/**
 * checkValidSequence function. Validates a sequences
 * @listens none
 * @param {string} - inputCode to validate
 * @param {array} - validCahrs acceptable chars on the sequence
 * @return {boolean} - true if sequence is valid, false otherwise
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function checkValidSequence (inputCode, validChars) {
    valid = true; 
    console.log(validChars.includes(inputCode.charAt(0).toUpperCase()));
    for (let i=0; i<inputCode.length; i++) {
        if (validChars.includes(inputCode.charAt(i).toUpperCase())== false) {  
            valid = false; 
            break;
        }  
    }
    console.log(valid)
    return valid;
}

//constants for validation
const DNACHARS =  ["A", "T", "G", "C"];
const AA = ["A", "R", "N", "D", "C", "Q", "E", "G", "H", "I", "L", "K", "M", "F", "P", "S", "T", "W", "Y", "V"]; 
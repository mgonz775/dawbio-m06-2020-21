/**
 * @file Manages the iframe code of index.html file.
 * @author Meritxell Ferret 
 * @version 1.0
 * @date 30/10/2020
 */


/**
 * submit function: Validates input data and call a parent function to proceed
 * @listens button.click event called submit
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function submit() {
    //parent
    var parent = window.parent.document;

    //input data
    var typeMolecule = document.getElementById("sel1").value;
    var numElem = document.getElementById("num").value;
    
    //validate
    if (numElem.toString().trim() !="" && !this.isNaN(numElem) && numElem > 0) {
        window.parent.introduceToDataBase();
    }else {
        document.getElementById("message").innerText="Not a number";
    }
};




/**
 * @file Manages the popUp window code of index.html file.
 * @author Meritxell Ferret 
 * @version 1.0
 * @date 30/10/2020
 */

/**
 * LoadData function: Constructs the summary table with its headers. 
 * @listens body.onload event
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function loadData() {

    //This parent object referencer index.html
    var parent = window.opener.document;

    //Referencer to iframe
    var myIframe = parent.getElementById("myIframe");

    //variables
    var typeMol = myIframe.contentWindow.document.getElementById("sel1").value;
    var numElem = myIframe.contentWindow.document.getElementById("num").value;

    //headers of the page
    document.getElementById("title").innerText += typeMol+" code";
    document.getElementById("date").innerText = timeStamp();
    document.getElementById("total").innerText += " "+numElem;

    //Get node text element to build the summary table
    for (let index = 0; index < numElem; index++) {
        var row = document.createElement("div");
        row.setAttribute("class", "row");

        var col1 = document.createElement("div");
        col1.setAttribute("class", "col-4");
        var txt1 = document.createTextNode(parent.getElementById("inputName"+index).value);
        col1.appendChild(txt1);
        
        var col2 = document.createElement("div");
        col2.setAttribute("class", "col-4");
        var txt2 = document.createTextNode(parent.getElementById("inputCode"+index).value);
        col2.appendChild(txt2);

        var col3 = document.createElement("div");
        col3.setAttribute("class", "col-4");
        if(parent.getElementById("inputCheckbox"+index).checked) {
            var txt3 = document.createTextNode("YES");
        } else var txt3 = document.createTextNode("NO");
        col3.appendChild(txt3);

        row.appendChild(col1);
        row.appendChild(col2);
        row.appendChild(col3);
        document.getElementById("table").appendChild(row);

    }
}

/**
 * printWindow function. Opens a dialog to print the page
 * @listens button.click event called printWindow
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function printWindow() {
    window.print();
}

/**
 * toClose function. Close the popUp window.
 * @listens button.click event called toClose
 * @param none
 * @return none
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function toClose() {
    window.close();
}

/**
 * timeStamp function. Gets the current day and time with readeable format
 * @listens none
 * @param none
 * @return {string} - the timestamp
 * @author Meritxell Ferret
 * @date 30/10/2020
 */
function timeStamp() {
    var currentdate = new Date(); 
    return datetime = "Time: " + currentdate.getDate() + "/"
                + (currentdate.getMonth()+1)  + "/" 
                + currentdate.getFullYear() + " @ "  
                + currentdate.getHours() + ":"  
                + currentdate.getMinutes() + ":" 
                + currentdate.getSeconds();
}


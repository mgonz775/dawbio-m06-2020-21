/**
 * @file Manages the client-side code for index.html file.
 * @author Marisa González 
 * @version 1.0
 * @date 07/10/2020
 */

/**
 * Window onload function.
 * @listens window.onload event
 * @param none
 * @return none
 * @author Marisa González
 * @date 07/10/2020
 */

window.onload = function(){
    this.clickBack();
};

/**
 * Click button function.
 * @listens button.click event
 * @param none
 * @return none
 * @author Marisa González
 * @date 07/10/2020
 */
function clickButton() {
    //Hide introduction div
    //.style.display="none" >> HIDEs
    //.style.display="block" >> SHOWs
    document.getElementById("divInput")
        .style.display="none";
    document.getElementById("divResult")
        .style.display="block";

    var result = document.getElementById("txtNumber").value;

    if (result.toString().trim() == "") {
        //DIVs, Ps... don't use value property to fill
        //a text area. Instead two different properties 
        //are used:
        //-innerText: for plain text
        //-innerHTML: for HTML-rich text
        document.getElementById("pResult").innerHTML=   
        "<B>Please, introduce a valid number</B>";

    } else {
        if (isNaN(result)) {
            document.getElementById("pResult")
            .innerText= 
            "You haven't introduced a number";
        } else {
            result = result * 2;
            document.getElementById("pResult")
            .innerText += result;
        }

    }
}

/**
 * Click back function.
 * @listens button.click event
 * @param none
 * @return none
 */

function clickBack(){
    document.getElementById("divInput")
        .style.display="block";
    document.getElementById("divResult")
        .style.display="none"; 
    document.getElementById("pResult")
        .innerText = "The result is: ";
    document.getElementById("txtNumber")
        .value = "";    
  
}


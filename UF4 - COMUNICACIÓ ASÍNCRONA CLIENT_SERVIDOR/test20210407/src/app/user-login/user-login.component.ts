import { Component, OnInit } from '@angular/core';
import { User } from '../model/User';
import { ReservationService } from '../services/reservation.service';

import { Router } from '@angular/router';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  validUserData: boolean = true;
  userConnected: User;

  constructor(private reservationService: ReservationService,
    private router: Router) { }

  ngOnInit(): void {
    this.userConnected = new User();
  }

  connection() {
    this.reservationService.userConnection
      (this.userConnected).subscribe(
        outPutData => {

          if (outPutData && Array.isArray(outPutData) &&
            outPutData.length > 0) {
            if (outPutData[0] === true) {
              this.validUserData = true;
              alert("User connected!");
            } else {
              this.validUserData = false;
              console.log("Error in UserLoginComponent(connection): \
                outPutData is not an Array"+ JSON.stringify(outPutData))
            }
          }
        },
        error => {
          alert("msg wrong");
          console.log("Error in UserLoginComponent(connection): \
              outPutData is not an Array"+ JSON.stringify(error))
        }
      );
  }

  register(){
    this.router.navigate(['user-management']);
  }
}



export class User{
    private id: number;
    private name: string;
    private surname1: string;
    private nick: string;
    private password: string;
    private image: string;


	constructor($id?: number, $name?: string, $surname1?: string, $nick?: string, $password?: string, $image?: string) {
		this.id = $id;
		this.name = $name;
		this.surname1 = $surname1;
		this.nick = $nick;
		this.password = $password;
		this.image = $image;
	}


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $name
     * @return {string}
     */
	public get $name(): string {
		return this.name;
	}

    /**
     * Getter $surname1
     * @return {string}
     */
	public get $surname1(): string {
		return this.surname1;
	}

    /**
     * Getter $nick
     * @return {string}
     */
	public get $nick(): string {
		return this.nick;
	}

    /**
     * Getter $password
     * @return {string}
     */
	public get $password(): string {
		return this.password;
	}

    /**
     * Getter $image
     * @return {string}
     */
	public get $image(): string {
		return this.image;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $name
     * @param {string} value
     */
	public set $name(value: string) {
		this.name = value;
	}

    /**
     * Setter $surname1
     * @param {string} value
     */
	public set $surname1(value: string) {
		this.surname1 = value;
	}

    /**
     * Setter $nick
     * @param {string} value
     */
	public set $nick(value: string) {
		this.nick = value;
	}

    /**
     * Setter $password
     * @param {string} value
     */
	public set $password(value: string) {
		this.password = value;
	}

    /**
     * Setter $image
     * @param {string} value
     */
	public set $image(value: string) {
		this.image = value;
	}

}
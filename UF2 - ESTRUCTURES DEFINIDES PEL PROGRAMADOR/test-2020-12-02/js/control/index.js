$(document).ready(function(){
    $("#butSubmit").click(function(e){
        //preventDefault will stop the regular behaviour of the event,
        //that would be to reload the form.
        e.preventDefault();

        var DNI, empName, surname, phone, birthday, salary;

        DNI = $("#DNI").val();
        empName = $("#empName").val();
        surname = $("#surname").val();
        phone = $("#phone").val();
        birthday = $("#birthdate").val();
        salary = $("#salary").val();

        var newEmployee = new Employee(DNI, empName, surname,
             phone, birthday, salary);
        
        console.log(newEmployee);

        //Setters and getter of the class work without parenthesis
        newEmployee.DNI = "46589454v";
        console.log(newEmployee.DNI);

    });
});
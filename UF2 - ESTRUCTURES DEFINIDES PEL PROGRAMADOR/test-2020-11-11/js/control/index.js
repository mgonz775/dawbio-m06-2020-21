// window.onload = function(){
//     alert ("window onload");
// }

$(document).ready(function(){
    $("a").click(function(event){
        // event.preventDefault();
        $(this).hide("1200");
        // $(this).remove();
        // $(this).attr("href", "http://www.google.com");
    });

    $("H1").click(function(){
        //Several instruccions over the same object, followed by a "."
        $(this).addClass("test").html("Hello world!");
    });

    $("#myList").click(function(){
        // $("#myList").append($("#myList li:first"));
        // $("#myList li:first").clone().appendTo("#myList");
        // $("#myList li:last").remove();

        //First method to iterate in Jquery
        // $("#myList li").each(function(){
        //     //$(this) is the element at every loop
        //     var li = $(this);
        //     console.log(li[0]);
        // });

        //Second method to iterate in Jquery
        //index is equivalent to the "i" item in a for
        //value = arr[index]
        var sum = 0;
        var arr = [1,2,3,4,5];
        $.each(arr, function(index, value){
            sum+=value;
        });
        console.log(sum);
    });

    $("#content").dblclick(function(){
        var myNewElement = $("<p>New element</p>");
        myNewElement.appendTo("#content");
    });
});
$(document).ready(function(){
    $("#butSubmit").click(function(event){
        
        event.preventDefault();

        var DNI, empName, surname, phone, birthday, salary;

        DNI = $("#DNI").val();
        empName = $("#empName").val();
        surname = $("#surname").val();
        phone = $("#phone").val();
        birthday = $("#birthdate").val();
        salary = $("#salary").val();

        var newEmployee = new Employee(DNI, empName, surname,
             phone, birthday, salary);
        
        console.log(newEmployee);
    });

    // Another way of capturing event in Jquery
    // $("whatever").on("event", "selector", function(){})ç
    // blur is triggered when an element has lost focus
    $("#myFS").on("blur", ".currency", function(){
        $(".currency").formatCurrency({
            region: 'es-ES',
            symbol: '$',
            positiveFormat: '%n %s',
            negativeFormat: '-%n %s',
            decimalSymbol: '.',
            digitGroupSymbol: ',',
            groupDigits: true
        });

    });
});
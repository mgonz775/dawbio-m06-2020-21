/**
*@name= dna-validator.component.ts
*@author= Quiles and Felipe
*@version= 1.0
*@description= Controller associated to dna-validator.component.html
*@date = 10-02-2021
*/
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Sequence } from '../model/sequence';
import { CookieService } from 'ngx-cookie-service';
import { DnaService } from '../services/dna.service';
@Component({
  selector: 'app-dna-validator',
  templateUrl: './dna-validator.component.html',
  styleUrls: ['./dna-validator.component.css']
})
export class DnaValidatorComponent implements OnInit {
  sequenceProp: Sequence;
  selector: String="DNA";
  sequence: String;
  sequenceID: String;
  submitFlag:boolean=false;
  cookieValue: string;
  flag: boolean = false;

  //PAGINATION

  //Filter properties
  IDSequence: String = "";
  typeSequence: String = "All";

  //Pagination properties
  currentPage: number;
  itemsPerPage: number;
  AllSequences: Sequence[]=[];
  SequenceFiltered: Sequence[]=[];
  SequenceSelected: Sequence;


  constructor(private titleService: Title, private cookieService: CookieService, private DnaService: DnaService) {
    this.titleService.setTitle("DNA-RNA Validator page");
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= In this function we load and initialize the variables for the page.
  *@date = 10-03-2021
  */
  ngOnInit(): void {
    this.SequenceSelected = new Sequence("","","");
    this.sequenceProp = new Sequence("","","");
    this.cookieValue = this.cookieService.get('Sequence');
    this.AllSequences = this.DnaService.generateRandomSequence();
    Object.assign(this.SequenceFiltered, this.AllSequences);
    this.itemsPerPage=10;
    this.currentPage=1;
    if(this.cookieValue != ""){
      Object.assign(this.SequenceSelected, JSON.parse(this.cookieValue));
    }
    this.submitFlag=false;
  }
  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function validates the sequence and add it to array.
  *@date = 10-03-2021
  */
  ValidateSequence(){
    this.submitFlag=true;
    this.cookieService.set('Sequence', JSON.stringify(this.SequenceSelected));
    this.SequenceFiltered.push(this.SequenceSelected);
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function recives a DNI object and charge the form.
  *@date = 10-03-2021
  */
  onClick(seq: Sequence){
    this.SequenceSelected = seq;
    this.flag = true;
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function remove the selected object from the DNI array.
  *@date = 10-03-2021
  */
  removeRes(seq: Sequence){
    this.AllSequences.splice
      (this.AllSequences.indexOf(seq),1);
    this.SequenceFiltered.splice
      (this.SequenceFiltered.indexOf(seq),1);
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function sets the flag to the false position so that the other form is moved and a new object must be initialized to reset the formulary.
  *@date = 10-03-2021
  */
  reset(){
    this.flag = false;
    this.SequenceSelected = new Sequence("","","");
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function applies the filters that we use for the table.
  *@date = 10-03-2021
  */
  filter(){
    this.SequenceFiltered = this.AllSequences.
      filter(seq => {
        let IDValid: boolean = false;
        let typeValid: boolean = false;

        // IndexOf: Returns the position of the 
        // first occurrence of a substring.
        // Otherwise returns -1
        if(this.IDSequence && this.IDSequence!=""){
          if(seq.$sequenceID.toLowerCase().indexOf
          (this.IDSequence.toLowerCase())!=-1){
            IDValid=true;
          }
        }else{
          IDValid=true;
        }

        if(this.typeSequence && this.typeSequence!=""){
          if(this.typeSequence == seq.$type || this.typeSequence == "All"){
            typeValid = true;
          }
        }
        return IDValid && typeValid;

      })
  }

}

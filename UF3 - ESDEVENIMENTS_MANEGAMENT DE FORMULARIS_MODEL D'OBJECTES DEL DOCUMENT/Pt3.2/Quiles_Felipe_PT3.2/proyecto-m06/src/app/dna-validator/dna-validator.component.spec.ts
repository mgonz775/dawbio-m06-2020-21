import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DnaValidatorComponent } from './dna-validator.component';

describe('DnaValidatorComponent', () => {
  let component: DnaValidatorComponent;
  let fixture: ComponentFixture<DnaValidatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DnaValidatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DnaValidatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

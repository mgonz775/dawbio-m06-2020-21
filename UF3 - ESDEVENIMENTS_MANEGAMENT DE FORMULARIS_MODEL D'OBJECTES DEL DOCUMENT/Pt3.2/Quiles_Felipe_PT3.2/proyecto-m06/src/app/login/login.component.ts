/**
*@name= login.component.ts
*@author= Quiles and Felipe
*@version= 1.0
*@description= Controller associated to login.component.html
*@date = 12-02-2021
*/
import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User } from '../model/user';
import { CookieService } from 'ngx-cookie-service';
import { LoginService } from '../services/login.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private titleService: Title, private cookieService: CookieService, private LoginService: LoginService) {
    this.titleService.setTitle("Login Page");
  }

  allUser: User[] = [];
  userSelected: User;

  flag: boolean;
  showEditForm: boolean = false;
  flagUsername: boolean;
  flagPassword: boolean;
  //PAGINATION
  //Filter properties
  nameFilter: string = "";
  usernameFilter: string = "";
  userFiltered: User[] = [];
  //Pagination properties
  currentPage: number;
  itemsPerPage: number;




  /**
  *
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= initalize data.
  *@date = 12-02-2021
  */
  ngOnInit(): void {
    this.itemsPerPage = 10;
    this.currentPage = 1;

    this.userSelected = new User("", "", "", "", "");
    this.createRandomUser();
    this.importObjectCookie();
  }


  /**
 *
 *@author= Quiles and Felipe
 *@version= 1.0
 *@description= Import objects from cookie if form has been sended previously.
 *@date = 12-02-2021
 */
  importObjectCookie() {
    if (this.cookieService.get("UserLogin") !== "") {
      Object.assign(this.userSelected, JSON.parse(this.cookieService.get("UserLogin")));
    }
  }

  /**
 *
 *@author= Quiles and Felipe
 *@version= 1.0
 *@description= Generate random users.
 *@date = 10-03-2021
 */
  createRandomUser() {
    this.allUser = this.LoginService.generateRandomUser();
    Object.assign(this.userFiltered, this.allUser);
  }

  /**
  *
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= When se form is submit call function validateUsernamePassword() to check de data
  *@date = 12-02-2021
  */
  login() {
    this.validateUsernamePassword();

  }
  /**
  *
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description=Check if introduces username and password exist and corresponds to themselves.If all are correct get the name, surname and email from the user
  *@date = 12-02-2021
  */
  validateUsernamePassword() {

    this.flag = false;

    for (let userElem in this.userFiltered) {
      this.flagUsername = false;
      this.flagPassword = false;
      if (this.userFiltered[userElem].$username == this.userSelected.$username) {
        this.flagUsername = true;
      }
      if (this.userFiltered[userElem].$password == this.userSelected.$password) {
        this.flagPassword = true;
      }
      if (this.flagUsername && this.flagPassword) {
        this.cookieService.set("UserLogin", JSON.stringify(this.userFiltered[userElem]))
        Object.assign(this.userSelected, this.userFiltered[userElem])
        this.flag = true;
        break;
      }
    }
  }
  onClick(user: User) {
    this.userSelected = user;
    this.showEditForm = true;
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function sets the flag to the false position so that the other form is moved and a new object must be initialized to reset the formulary.
  *@date = 10-03-2021
  */
  reset() {
    this.showEditForm = false;
    this.userSelected = new User("", "", "", "", "");
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function remove the selected object from the DNI array.
  *@date = 10-03-2021
  */
  removeUser(user: User) {
    this.allUser.splice
      (this.allUser.indexOf(user), 1);
    this.userFiltered.splice
      (this.userFiltered.indexOf(user), 1);
  }

  /**
    *@author= Quiles and Felipe
    *@version= 1.0
    *@description= Search object with the introduced filter
    *@date = 10-03-2021
    */
  filter() {
    this.userFiltered = this.allUser.
      filter(user => {
        let nameValid: boolean = false;
        let usernameValid: boolean = false;


        if (this.nameFilter && this.nameFilter != "") {
          if (user.$name.toLowerCase().indexOf
            (this.nameFilter.toLowerCase()) != -1) {
            nameValid = true;
          }
        } else {
          nameValid = true;
        }
        if (this.usernameFilter && this.usernameFilter != "") {

          if (user.$username.toLowerCase().indexOf(this.usernameFilter.toLowerCase()) != -1) {
            usernameValid = true;

          }
        } else {
          usernameValid = true;
        }

        return nameValid && usernameValid;

      })
  }
}

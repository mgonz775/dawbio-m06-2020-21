import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';

@Directive({
  selector: '[appMinLengthDNI]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MinLengthDNIDirective,
    multi: true
  }]
})
export class MinLengthDNIDirective {

  constructor() { }
  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {

    let validInput: boolean = false;
    
    //Firstly we check that formFieldToValidate is not null
    if (formFieldToValidate && formFieldToValidate.value > 9) {
      validInput = true;
    }
    return validInput ? null : { 'incorrectMinLength': true }
  }
}

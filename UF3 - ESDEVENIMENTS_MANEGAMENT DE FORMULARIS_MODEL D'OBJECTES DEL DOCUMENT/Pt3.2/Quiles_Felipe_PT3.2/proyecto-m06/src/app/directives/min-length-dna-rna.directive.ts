import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';

@Directive({
  selector: '[appMinLengthDNARNA]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MinLengthDNARNADirective,
    multi: true
  }]
})
export class MinLengthDNARNADirective {

  constructor() { }
  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {

    let validInput: boolean = false;
    
    //Firstly we check that formFieldToValidate is not null
    if (formFieldToValidate && formFieldToValidate.value && formFieldToValidate.value.length >=9) {
      validInput = true;
    }
    return validInput ? null : { 'incorrectMinLength': true }
  }
}
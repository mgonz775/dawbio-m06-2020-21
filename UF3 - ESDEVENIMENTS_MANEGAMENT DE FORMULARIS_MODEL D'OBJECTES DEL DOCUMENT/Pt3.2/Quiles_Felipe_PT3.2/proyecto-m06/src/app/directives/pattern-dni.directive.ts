import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';

@Directive({
  selector: '[appPatternDNI]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: PatternDNIDirective,
    multi: true
  }]
})
export class PatternDNIDirective {

  LetterDNI_REGEXP = new RegExp ("[TRWAGMYFPDXBNJZSQVHLCKET]");

  constructor() { }

  /**
     * @description
     * Method that performs validation against the provided control.
     *
     * @param {AbstractControl} formFieldToValidate - The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */

  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {
    let validInput: boolean = false;

    if (formFieldToValidate && formFieldToValidate.value) {
      validInput = this.LetterDNI_REGEXP.test(formFieldToValidate.value)
    }
    return validInput ? null : { 'incorrectLetter': true };
  }

}


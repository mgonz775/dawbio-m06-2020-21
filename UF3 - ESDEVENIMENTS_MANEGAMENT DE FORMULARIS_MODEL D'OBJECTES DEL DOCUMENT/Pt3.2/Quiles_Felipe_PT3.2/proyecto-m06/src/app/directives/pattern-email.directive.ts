import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';
@Directive({
  selector: '[appPatternEmail]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: PatternEmailDirective,
    multi: true
  }]
})

export class PatternEmailDirective {

  patternEmail = new RegExp ("[A-Za-z0-9._%-]+@[A-Za-z0-9._%-]+\\.[a-z]{2,3}");

  constructor() { }

  /**
     * @description
     * Method that performs validation against the provided control.
     *
     * @param {AbstractControl} formFieldToValidate - The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */

  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {
    let validInput: boolean = false;

    if (formFieldToValidate && formFieldToValidate.value) {
      validInput = this.patternEmail.test(formFieldToValidate.value)
    }
    return validInput ? null : { 'invalidEmailPattern': true };
  }

}
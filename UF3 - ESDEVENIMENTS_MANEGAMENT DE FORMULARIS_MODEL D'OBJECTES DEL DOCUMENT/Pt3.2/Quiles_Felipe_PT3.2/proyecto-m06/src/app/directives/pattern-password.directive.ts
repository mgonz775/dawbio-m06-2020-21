
import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';
@Directive({
  selector: '[appPatternPassword]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: PatternPasswordDirective,
    multi: true
  }]
})

export class PatternPasswordDirective {

  patternEmail = new RegExp ("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$");

  constructor() { }

  /**
     * @description
     * Method that performs validation against the provided control.
     *
     * @param {AbstractControl} formFieldToValidate - The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */

  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {
    let validInput: boolean = false;

    if (formFieldToValidate && formFieldToValidate.value) {
      validInput = this.patternEmail.test(formFieldToValidate.value)
    }
    return validInput ? null : { 'invalidPasswordPattern': true };
  }

}
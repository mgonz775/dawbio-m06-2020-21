import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl }
  from '@angular/forms';

@Directive({
  selector: '[appMaxLengthDNI]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: MaxLenghtDNIDirective,
    multi: true
  }]
})
export class MaxLenghtDNIDirective {

  constructor() { }

  validate(formFieldToValidate: AbstractControl): { [key: string]: any } {

    let validInput: boolean = false;
    
    //Firstly we check that formFieldToValidate is not null
    if (formFieldToValidate && formFieldToValidate.value < 100000000) {
      validInput = true;
    }
    return validInput ? null : { 'incorrectMaxLength': true }
  }

}

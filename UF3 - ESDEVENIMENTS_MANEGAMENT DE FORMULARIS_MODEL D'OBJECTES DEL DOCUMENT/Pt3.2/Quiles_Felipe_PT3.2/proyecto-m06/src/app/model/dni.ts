export class DNI {

    private fullName: string;
    private dniNumbers: number;
    private dniLetter: string;

    constructor($fullName: string, $dniNumbers: number, $dniLetter: string) {
		this.fullName = $fullName;
		this.dniNumbers = $dniNumbers;
		this.dniLetter = $dniLetter;
	}

    /**
     * Getter $fullName
     * @return {string}
     */
	public get $fullName(): string {
		return this.fullName;
	}

    /**
     * Getter $dniNumbers
     * @return {string}
     */
	public get $dniNumbers(): number {
		return this.dniNumbers;
	}

    /**
     * Getter $dniLetter
     * @return {string}
     */
	public get $dniLetter(): string {
		return this.dniLetter;
	}

    /**
     * Setter $fullName
     * @param {string} value
     */
	public set $fullName(value: string) {
		this.fullName = value;
	}

    /**
     * Setter $dniNumbers
     * @param {string} value
     */
	public set $dniNumbers(value: number) {
		this.dniNumbers = value;
	}

    /**
     * Setter $dniLetter
     * @param {string} value
     */
	public set $dniLetter(value: string) {
		this.dniLetter = value;
	}

}

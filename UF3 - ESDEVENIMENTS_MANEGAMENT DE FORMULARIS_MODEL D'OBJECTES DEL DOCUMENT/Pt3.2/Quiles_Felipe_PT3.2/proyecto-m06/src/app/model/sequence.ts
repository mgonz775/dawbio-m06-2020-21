export class Sequence {

    private sequenceID: String;
    private type: String;
    private sequence: String;


	constructor($sequenceID: String, $type: String, $sequence: String) {
		this.sequenceID = $sequenceID;
		this.type = $type;
		this.sequence = $sequence;
	}


    /**
     * Getter $sequenceID
     * @return {String}
     */
	public get $sequenceID(): String {
		return this.sequenceID;
	}

    /**
     * Getter $type
     * @return {String}
     */
	public get $type(): String {
		return this.type;
	}

    /**
     * Getter $sequence
     * @return {String}
     */
	public get $sequence(): String {
		return this.sequence;
	}

    /**
     * Setter $sequenceID
     * @param {String} value
     */
	public set $sequenceID(value: String) {
		this.sequenceID = value;
	}

    /**
     * Setter $type
     * @param {String} value
     */
	public set $type(value: String) {
		this.type = value;
	}

    /**
     * Setter $sequence
     * @param {String} value
     */
	public set $sequence(value: String) {
		this.sequence = value;
	}

}

export class User {


    private name: string;
    private surname: string;
    private email: string;
    private username: string;
    private password: string;


    constructor(name: string, surname: string, email: string, username: string, password: string) {
        this.name = name
        this.surname = surname
        this.email = email
        this.username = username
        this.password = password
    }

    /**
 * Getter $name
 * @return {string}
 */
    public get $name(): string {
        return this.name;
    }

    /**
     * Getter $surname
     * @return {string}
     */
    public get $surname(): string {
        return this.surname;
    }

    /**
     * Getter $email
     * @return {string}
     */
    public get $email(): string {
        return this.email;
    }

    /**
     * Getter $username
     * @return {string}
     */
    public get $username(): string {
        return this.username;
    }

    /**
     * Getter $password
     * @return {string}
     */
    public get $password(): string {
        return this.password;
    }

    /**
     * Setter $name
     * @param {string} value
     */
    public set $name(value: string) {
        this.name = value;
    }

    /**
     * Setter $surname
     * @param {string} value
     */
    public set $surname(value: string) {
        this.surname = value;
    }

    /**
     * Setter $email
     * @param {string} value
     */
    public set $email(value: string) {
        this.email = value;
    }

    /**
     * Setter $username
     * @param {string} value
     */
    public set $username(value: string) {
        this.username = value;
    }

    /**
     * Setter $password
     * @param {string} value
     */
    public set $password(value: string) {
        this.password = value;
    }
}

import { importType } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';
import { User } from '../model/user'
@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor() { }
  generateRandomUser(): User[] {
    let userArray: User[] = [];
    let randomName: string;
    let randomSurname: string;
    let randomEmail: string;
    let randomUsername: string;
    let randomPassword: string;

    let user: User;

    for (let i = 0; i < 30; i++) {
      if (i % 2 == 0) {
        randomName = "NameUser" + i;
        randomSurname = "SurnameUser" + i;
        randomEmail = "EmailUser" + i + "@gmail.com";
        randomUsername = "user" + i;
        randomPassword = "User" + i;
      } else {
        randomName = "Name" + i;
        randomSurname = "";
        randomEmail = "UserEmail" + i + "@gmail.com";
        randomUsername = "user" + i;
        randomPassword = "12345";
      }
      user = new User(randomName, randomSurname, randomEmail, randomUsername, randomPassword)
      userArray.push(user);
    }
    return userArray;
  }
}

import { Injectable } from '@angular/core';
import { DNI } from '../model/dni';

@Injectable({
  providedIn: 'root'
})
export class DNIService {

  constructor() { }

  generateRandomDNI(): DNI[]{
    let dnis: DNI[] =[];

    let randomName: string;
    let randomNumbersDNI: number;
    let rest: number;
    let letterDNI: string;
    let validLetters: String = "TRWAGMYFPDXBNJZSQVHLCKET";

    let dni: DNI;

    for (let i = 0; i < 299; i++) {
      randomName="Name"+i;
      randomNumbersDNI = Math.floor(Math.random()*(100000000-10)+10);
      rest = (randomNumbersDNI % 23);
      letterDNI = validLetters[rest];
      dni = new DNI(randomName,randomNumbersDNI,letterDNI);
      dnis.push(dni);
    }
    return dnis;
  }


}


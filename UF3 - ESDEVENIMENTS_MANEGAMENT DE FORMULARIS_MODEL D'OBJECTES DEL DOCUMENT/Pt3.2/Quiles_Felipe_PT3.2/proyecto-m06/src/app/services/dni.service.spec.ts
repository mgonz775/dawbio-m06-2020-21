import { TestBed } from '@angular/core/testing';

import { DNIService } from './dni.service';

describe('DNIServiceService', () => {
  let service: DNIService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DNIService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

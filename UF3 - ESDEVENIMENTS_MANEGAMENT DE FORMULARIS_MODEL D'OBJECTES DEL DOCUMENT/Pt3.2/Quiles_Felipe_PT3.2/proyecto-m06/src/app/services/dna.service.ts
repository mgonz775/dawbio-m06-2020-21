import { Injectable } from '@angular/core';
import { Sequence } from '../model/sequence';
@Injectable({
  providedIn: 'root'
})
export class DnaService {

  constructor() { }

  generateRandomSequence(): Sequence[]{
    let sequences: Sequence[] =[];

    let randomID: string;
    let randomSequence: string = "";
    let randomTypeOption: number;
    let randomType: String;

    let sequence: Sequence;

    for (let i = 0; i < 299; i++) {
      randomID="Sequence"+i;
      const charactersDNA = 'ACTG';
      const charactersRNA = 'ACUG';
      randomTypeOption = Math.floor(Math.random()*2);
      if(i%5==0){
        randomType="DNA";
        for (let i = 0; i < 12; i++) {
          randomSequence += charactersDNA.charAt(Math.floor(Math.random() * 4));
        }
      }else{
        randomType="RNA";
        for (let i = 0; i < 12; i++) {
          randomSequence += charactersRNA.charAt(Math.floor(Math.random() * 4));
        }
      }
      sequence = new Sequence(randomID,randomType,randomSequence);
      sequences.push(sequence);
      randomSequence="";
    }
    return sequences;
  }

}

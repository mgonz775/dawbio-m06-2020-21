import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DnaValidatorComponent } from './dna-validator/dna-validator.component';
import { DniValidatorComponent } from './dni-validator/dni-validator.component';
import { MainPageComponent } from './main-page/main-page.component';
import { MinLengthDNIDirective } from './directives/min-length-dni.directive';
import { MaxLenghtDNIDirective } from './directives/max-lenght-dni.directive';
import { PatternDNIDirective } from './directives/pattern-dni.directive';
import { PatternDnaDirective } from './directives/pattern-dna.directive';
import { MinLengthDNARNADirective } from './directives/min-length-dna-rna.directive';
import { PatternRNADirective } from './directives/pattern-rna.directive';
import { PatternEmailDirective } from './directives/pattern-email.directive';
import { PatternPasswordDirective } from './directives/pattern-password.directive';

import { CookieService } from 'ngx-cookie-service';
import { NgxPaginationModule } from 'ngx-pagination';

const appRoutes: Routes = [{ path: "dna-validator", component: DnaValidatorComponent },
{ path: "dni-validator", component: DniValidatorComponent },
{ path: "registration", component: RegistrationComponent },
{ path: "login", component: LoginComponent },
{ path: '' , component:MainPageComponent},]

@NgModule({
  declarations: [
    AppComponent,
    RegistrationComponent,
    LoginComponent,
    DnaValidatorComponent,
    DniValidatorComponent,
    MainPageComponent,
    MinLengthDNIDirective,
    MaxLenghtDNIDirective,
    PatternDNIDirective,
    PatternDnaDirective,
    MinLengthDNARNADirective,
    PatternRNADirective,
    PatternEmailDirective,
    PatternPasswordDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgxPaginationModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }

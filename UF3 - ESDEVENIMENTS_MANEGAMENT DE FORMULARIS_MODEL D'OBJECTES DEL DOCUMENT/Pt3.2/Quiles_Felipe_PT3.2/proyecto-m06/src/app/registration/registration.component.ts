/**
*@name= login.component.ts
*@author= Quiles and Felipe
*@version= 1.0
*@description= Controller associated to login.component.html
*@date = 12-02-2021
*/

import { Component, OnInit } from '@angular/core';
import { Title } from "@angular/platform-browser";
import { User } from '../model/user';
import { CookieService } from 'ngx-cookie-service';
import { RegisterService } from '../services/register.service';

//import { R } from '../services/dni.service';
@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  flag: boolean;
  showEditForm: boolean = false;

  //PAGINATION

  //Filter properties
  emailFilter: string = "";
  surnameFilter: string = "";
  passwordFilter: number = 8;
  //Pagination properties
  currentPage: number;
  itemsPerPage: number;
  allUser: User[] = [];
  userFiltered: User[] = [];
  userSelected: User;
  constructor(private titleService: Title, private cookieService: CookieService, private RegisterService: RegisterService) {
    this.titleService.setTitle("Register Page");
  }
  /**
    *
    *@author= Quiles and Felipe
    *@version= 1.0
    *@description= Initalize data.
    *@date = 12-02-2021
    */
  ngOnInit(): void {
    this.itemsPerPage = 10;
    this.currentPage = 1;

    this.userSelected = new User("", "", "", "", "");
    this.createRandomUser();
    this.cookiesObject();
  }
  /**
   *
   *@author= Quiles and Felipe
   *@version= 1.0
   *@description= Generate random users.
   *@date = 10-03-2021
   */
  createRandomUser() {
    this.allUser = this.RegisterService.generateRandomUser();
    Object.assign(this.userFiltered, this.allUser);
  }
  cookiesObject() {
    if (this.cookieService.get("UserRegister") !== "") {
      Object.assign(this.userSelected, JSON.parse(this.cookieService.get("UserRegister")));
    }
  }
  /**
    *
    *@author= Quiles and Felipe
    *@version= 1.0
    *@description= Calls the function dataValidationRegister() and check if is false.If is false 
    *@date = 12-02-2021
    */
  register() {
    this.dataValidationRegister()
    if (!this.flag) {
      this.userFiltered.push(this.userSelected);
      this.cookieService.set("UserRegister", JSON.stringify(this.userSelected));
    }

  }
  /**
    *
    *@author= Quiles and Felipe
    *@version= 1.0
    *@description= check the array of objects if the username already exist
    *@date = 12-02-2021
    */
  dataValidationRegister() {
    for (let userElem in this.userFiltered) {
      if (this.userFiltered[userElem].$username == this.userSelected.$username) {
        this.flag = true;
        break;
      } else {
        this.flag = false;
      }
    }
  }

  onClick(user: User) {
    this.userSelected = user;
    this.showEditForm = true;
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function sets the flag to the false position so that the other form is moved and a new object must be initialized to reset the formulary.
  *@date = 10-03-2021
  */
  reset() {
    this.showEditForm = false;
    this.userSelected = new User("", "", "", "", "");
  }
  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function remove the selected object from the DNI array.
  *@date = 10-03-2021
  */
  removeUser(user: User) {
    this.allUser.splice
      (this.allUser.indexOf(user), 1);
    this.userFiltered.splice
      (this.userFiltered.indexOf(user), 1);
  }
  /**
   *@author= Quiles and Felipe
   *@version= 1.0
   *@description= Search object with the introduced filter
   *@date = 10-03-2021
   */
  filter() {
    this.userFiltered = this.allUser.
      filter(user => {
        let nameValid: boolean = false;
        let surnameValid: boolean = false;
        let passwordValid: boolean = false;


        if (this.emailFilter && this.emailFilter != "") {
          if (user.$email.toLowerCase().indexOf(this.emailFilter.toLowerCase()) != -1) {
            nameValid = true;
          }
        } else {
          nameValid = true;
        }
        if (this.surnameFilter && this.surnameFilter != "") {

          if (user.$username.toLowerCase().indexOf(this.surnameFilter.toLowerCase()) != -1) {
            surnameValid = true;
          }
        } else {
          surnameValid = true;
        }
        passwordValid = (user.$password.length >= this.passwordFilter);

        return nameValid && surnameValid && passwordValid;

      })
  }
}

/**
*@name= dni-validator.component.ts
*@author= Quiles and Felipe
*@version= 1.0
*@description= Controller associated to  dni-validator.component.html
*@date = 31-01-2021
*/
import { Component, OnInit} from '@angular/core';
import { DNI } from '../model/dni';
import { Title } from '@angular/platform-browser';
import { CookieService } from 'ngx-cookie-service';
import { DNIService } from '../services/dni.service';
@Component({
  selector: 'app-dni-validator',
  templateUrl: './dni-validator.component.html',
  styleUrls: ['./dni-validator.component.css']
})
export class DniValidatorComponent implements OnInit {



  introducedLetter: string = "";
  introducedNumberDNI: number;
  validLetters: String = "TRWAGMYFPDXBNJZSQVHLCKET";
  rest: number;
  realLetter: String;
  checked: boolean;
  name: string;
  cookieValue: string;
  DNIProp: DNI;
  flag: boolean = false;

  //PAGINATION

  //Filter properties
  totalDNINumbers: number = 99999999;
  nameFilter: string = "";

  //Pagination properties
  currentPage: number;
  itemsPerPage: number;
  AllDNI: DNI[]=[];
  DNIFiltered: DNI[]=[];
  DNISelected: DNI;
  

  constructor(private titleService: Title, private cookieService: CookieService, private DNIService: DNIService) {
    this.titleService.setTitle("DNI Validator Page");
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= In this function we load and initialize the variables for the page.
  *@date = 10-03-2021
  */
  ngOnInit(): void {
    this.DNIProp = new DNI("",0,"");
    this.DNISelected = new DNI("",0,"");
    this.AllDNI = this.DNIService.generateRandomDNI();
    Object.assign(this.DNIFiltered, this.AllDNI);
    this.cookieValue = this.cookieService.get('DNI');
    this.itemsPerPage=10;
    this.currentPage=1;
    if(this.cookieValue != ""){
      Object.assign(this.DNISelected, JSON.parse(this.cookieValue));
    }

  }
  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function validates that the ID number corresponds to the letter and add it to array.
  *@date = 10-03-2021
  */
  ValidateDNI(): void {
    this.rest = (this.DNISelected.$dniNumbers % 23);
    this.realLetter = this.validLetters[this.rest];
    if (this.realLetter == this.DNISelected.$dniLetter) {
      this.checked = true;
      console.log("The DNI is valid");
      console.log(new DNI(this.DNISelected.$fullName, this.DNISelected.$dniNumbers, this.DNISelected.$dniLetter));
      this.DNIProp = new DNI(this.DNISelected.$fullName, this.DNISelected.$dniNumbers, this.DNISelected.$dniLetter);
      this.cookieService.set('DNI', JSON.stringify(this.DNIProp));
      this.DNIFiltered.push(this.DNIProp);
      
    } else {
      this.checked = false;
      console.log("The DNI is invalid");
    }
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function applies the filters that we use for the table.
  *@date = 10-03-2021
  */
  filter(){
    this.DNIFiltered = this.AllDNI.
      filter(DNI => {
        let numberValid: boolean = false;
        let nameValid: boolean = false;

        numberValid=(DNI.$dniNumbers<=this.totalDNINumbers);

        // IndexOf: Returns the position of the 
        // first occurrence of a substring.
        // Otherwise returns -1
        if(this.nameFilter && this.nameFilter!=""){
          if(DNI.$fullName.toLowerCase().indexOf
          (this.nameFilter.toLowerCase())!=-1){
            nameValid=true;
          }
        }else{
          nameValid=true;
        }
        
        return nameValid && numberValid;

      })
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function recives a DNI object and charge the form.
  *@date = 10-03-2021
  */
  onClick(dni: DNI){
    this.DNISelected = dni;
    this.flag = true;
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function sets the flag to the false position so that the other form is moved and a new object must be initialized to reset the formulary.
  *@date = 10-03-2021
  */
  reset(){
    this.flag = false;
    this.DNISelected = new DNI("",0,"");
  }

  /**
  *@author= Quiles and Felipe
  *@version= 1.0
  *@description= This function remove the selected object from the DNI array.
  *@date = 10-03-2021
  */
  removeRes(dni: DNI){
    this.AllDNI.splice
      (this.AllDNI.indexOf(dni),1);
    this.DNIFiltered.splice
      (this.DNIFiltered.indexOf(dni),1);
  }


}

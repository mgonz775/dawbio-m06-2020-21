import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DniValidatorComponent } from './dni-validator.component';

describe('DniValidatorComponent', () => {
  let component: DniValidatorComponent;
  let fixture: ComponentFixture<DniValidatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DniValidatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DniValidatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, Input }
 from '@angular/core';
import { DatePipe } from '@angular/common';

import { Reservation } from '../model/Reservation';
import { ReservationTime } from '../model/ReservationTime';
import { TablePreference } from '../model/TablePreference';
import { SpecialRequests } from '../model/SpecialRequests';

import { ReservationService } from '../services/reservation.service';

import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css'],
  providers:[DatePipe]
})
export class ReservationEntryComponent implements OnInit {
  //Properties
  @Input() reservation: Reservation;
  recommendations: String[][] = [["Restaurants", "Rates"], ["El celler", "***"], ["Diverxo", "**"]];
  reservationTimes: ReservationTime[] = [];
  tablePreferences: TablePreference[] = [];
  specialRequests : SpecialRequests[] = [];

  cookieObj: any;

  @ViewChild('reservationEntryForm') 
  reservationEntryForm: HTMLFormElement;

  @ViewChild('divCheckBox')
   divCheckBox: HTMLFormElement;

  constructor(private datePipe : DatePipe,
    private cookieService: CookieService,
    private reservationService: ReservationService) { }

  ngOnInit() {
    this.reservationTimes = 
      this.reservationService.createReservationTimes();
    this.tablePreferences = 
      this.reservationService.createReservationPreferences();
    this.specialRequests =
      this.reservationService.createSpecialRequests();
    this.initializeForm();    
    this.getCookie();
  }

  ngAfterViewInit(){
    this.getCookieCheckBox();
  }

  reservationEntry(): void {
    //this.cookieService.delete("reservation");
    this.cookieService.set("reservation", JSON.stringify(this.reservation));
    console.log(this.reservation);
    
  }
 

  initializeForm(){
    if (this.reservationEntryForm) {
      this.reservationEntryForm.reset();
      this.reservationEntryForm.form.markAsPristine();
      this.reservationEntryForm.form.markAsUntouched();
    }
    
    //If this.reservation is null is because
    //no input param has been received.
    //Otherwise, we should be careful not to
    //smash the parameter
    if(!this.reservation){
      this.reservation = new Reservation();

      this.reservation.reservationDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      this.reservation.reservationTime = this.reservationTimes[2];
      this.reservation.tablePreference = this.tablePreferences[0];
      this.reservation.specialRequests=[];
      this.reservation.totalPrice=18;
     
    }
    
  }

  // This method is commented due to the interference
  // with the Input parameter Reservation
  // Use it when you need cookies implementation
  getCookie(){
    if(this.cookieService.check("reservation")){
      this.cookieObj = JSON.parse(this.cookieService.get("reservation"));
       /**
        * Copy the values of all of the enumerable own properties from one or more source objects to a
        * target object. Returns the target object.
        * @param target The target object to copy to.
        * @param source The source object from which to copy properties.
        */
      Object.assign(this.reservation, this.cookieObj);
      
      
      this.reservation.reservationTime = 
        this.reservationTimes[this.cookieObj._reservationTime.id];

      this.reservation.tablePreference = 
        this.tablePreferences[this.cookieObj._tablePreference.id];

    }
  }

  getCookieCheckBox(){
    if(this.cookieObj){
      this.reservation.specialRequests = [];
      //We create new Objects SpecialRequests because the
      //Object.assign method doesn't give the full method structure of the class
      for(let spAux of this.cookieObj._specialRequests){
        this.reservation.specialRequests.push(
          new SpecialRequests(spAux.id, spAux.request, spAux.price));
          this.divCheckBox.nativeElement.
           children[spAux.id].children[0].checked=true;       
      }
    }
  }
  
}






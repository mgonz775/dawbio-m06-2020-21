import { TestBed } from '@angular/core/testing';

import { PruebaServicioService } from './prueba-servicio.service';

describe('PruebaServicioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PruebaServicioService = TestBed.get(PruebaServicioService);
    expect(service).toBeTruthy();
  });
});

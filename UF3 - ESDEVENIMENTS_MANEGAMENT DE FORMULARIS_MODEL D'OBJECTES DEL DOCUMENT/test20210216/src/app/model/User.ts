export class User{
    private _name: string;

    //The question mark informs the parameter is optional
	constructor(name?: string) {
		this._name = name;
	}
  

    /**
     * Getter name
     * @return {string}
     */
	public get name(): string {
		return this._name;
	}

    /**
     * Setter name
     * @param {string} value
     */
	public set name(value: string) {
		this._name = value;
	}

}
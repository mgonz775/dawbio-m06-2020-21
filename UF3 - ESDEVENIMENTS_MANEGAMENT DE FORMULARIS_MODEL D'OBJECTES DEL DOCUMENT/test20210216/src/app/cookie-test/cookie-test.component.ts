import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { User } from '../model/User';

@Component({
  selector: 'app-cookie-test',
  templateUrl: './cookie-test.component.html',
  styleUrls: ['./cookie-test.component.css']
})
export class CookieTestComponent implements OnInit {
  
  userProp: User;
  userLogged: boolean;
  cookieValue: string;

  constructor(private cookieService: CookieService) { }

  ngOnInit() {
    this.userProp = new User();
    this.cookieValue = this.cookieService.get('UserApp');

    if(this.cookieValue == ""){
      this.userLogged=false;
    }else{
      //JSON.parse is the opposite of JSON.string
      //The second transforms a complex object into a JSON string
      //The first does the inverse.
      //Object.assing copies, one by one, all the properties of a source object into a target object
      Object.assign(this.userProp, JSON.parse(this.cookieValue)) ;
      this.userLogged=true;
    }
  }

  sendForm(){  
    this.cookieService.set('UserApp', 
      JSON.stringify(this.userProp));
  }

  logout(){
    this.cookieService.delete('UserApp');
    this.userLogged = false;
    this.userProp=new User();
  }
}

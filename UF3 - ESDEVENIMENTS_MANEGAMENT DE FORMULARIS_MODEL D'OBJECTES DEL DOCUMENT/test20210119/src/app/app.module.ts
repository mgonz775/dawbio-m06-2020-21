import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { ReservationEntryComponent } from './reservation-entry/reservation-entry.component';
import { ReservationManagementComponent } from './reservation-management/reservation-management.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';

//4 routes:
//1 for each component (3 components)
//1 for default behaviour
const appRoutes: Routes = [
  //The next path is equivalent to http://localhost:4200/new-reservation
  { path: 'new-reservation' , component: ReservationEntryComponent},
  { path: 'reservation-management' , component: ReservationManagementComponent},
  //The next path is equivalent to http://localhost:4200
  { path: '' , component: ReservationEntryComponent},
  //** is equivalent to different than before */
  { path: '**' , component: PageNotFoundComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    ReservationEntryComponent,
    ReservationManagementComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

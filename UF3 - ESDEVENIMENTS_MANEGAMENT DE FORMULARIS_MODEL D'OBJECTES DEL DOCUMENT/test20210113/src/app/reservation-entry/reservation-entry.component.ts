import { Component, OnInit } from '@angular/core';
import { Reservation } from '../model/reservation';

@Component({
  selector: 'app-reservation-entry',
  templateUrl: './reservation-entry.component.html',
  styleUrls: ['./reservation-entry.component.css']
})
export class ReservationEntryComponent implements OnInit {

  objReservation: Reservation;
  recommendations: String[][] = [["Restaurants", "Rates"],
    ["El celler","***"], ["Diverxo", "***"]];

  constructor() { }

  ngOnInit(): void {
    this.objReservation = new Reservation(0, "", "", "", "", new Date(), "");
    this.objReservation.$name = "Marisa";
  }

  reservationEntry(): void{
    console.log(this.objReservation);
  }

}

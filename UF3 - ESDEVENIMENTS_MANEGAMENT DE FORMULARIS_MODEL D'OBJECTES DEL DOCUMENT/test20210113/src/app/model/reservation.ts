export class Reservation {
    private id: number;
    private name: String;
    private surname: String;
    private email: String;
    private phone: String;
    private reservationDate: Date;
    private reservationTime: String;


  constructor(
    id: number, 
    name: String, 
    surname: String, 
    email: String, 
    phone: String, 
    reservationDate: Date, 
    reservationTime: String
) {
    this.id = id
    this.name = name
    this.surname = surname
    this.email = email
    this.phone = phone
    this.reservationDate = reservationDate
    this.reservationTime = reservationTime
  }


    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $name
     * @return {String}
     */
	public get $name(): String {
		return this.name;
	}

    /**
     * Getter $surname
     * @return {String}
     */
	public get $surname(): String {
		return this.surname;
	}

    /**
     * Getter $email
     * @return {String}
     */
	public get $email(): String {
		return this.email;
	}

    /**
     * Getter $phone
     * @return {String}
     */
	public get $phone(): String {
		return this.phone;
	}

    /**
     * Getter $reservationDate
     * @return {Date}
     */
	public get $reservationDate(): Date {
		return this.reservationDate;
	}

    /**
     * Getter $reservationTime
     * @return {String}
     */
	public get $reservationTime(): String {
		return this.reservationTime;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $name
     * @param {String} value
     */
	public set $name(value: String) {
		this.name = value;
	}

    /**
     * Setter $surname
     * @param {String} value
     */
	public set $surname(value: String) {
		this.surname = value;
	}

    /**
     * Setter $email
     * @param {String} value
     */
	public set $email(value: String) {
		this.email = value;
	}

    /**
     * Setter $phone
     * @param {String} value
     */
	public set $phone(value: String) {
		this.phone = value;
	}

    /**
     * Setter $reservationDate
     * @param {Date} value
     */
	public set $reservationDate(value: Date) {
		this.reservationDate = value;
	}

    /**
     * Setter $reservationTime
     * @param {String} value
     */
	public set $reservationTime(value: String) {
		this.reservationTime = value;
	}

    

}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { DNAValidatorDirDirective } from './directives/dnavalidator-dir.directive';

@NgModule({
  declarations: [
    AppComponent,
    DNAValidatorDirDirective
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

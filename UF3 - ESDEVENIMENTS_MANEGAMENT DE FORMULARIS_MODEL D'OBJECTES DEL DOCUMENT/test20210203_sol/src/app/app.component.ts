import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'DNAValidator';
  codeTestProp: String = "";

  constructor() { }

  ngOnInit() {}

  sendForm(){
    console.log('Form sent');
  }
}

import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } 
  from '@angular/forms';

@Directive({
  selector: '[appInputMinLength]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: InputMinLengthDirective,
    multi:true
  }]
})
export class InputMinLengthDirective {

  constructor() { }

  //returns null if everything is OK
  //error map otherwise
  validate(formFieldToValidate: AbstractControl): 
    {[key: string]: any}{
    
    let validInput: boolean = false;

    console.log("entra");
    //Firstly we check that formFieldToValidate is not null
    if(formFieldToValidate && formFieldToValidate.value.length > 5){
      validInput=true;
    }

    return validInput ? null : { 'isNotCorrect':true } 
    }
}

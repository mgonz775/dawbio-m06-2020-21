import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-test-ngif',
  templateUrl: './test-ngif.component.html',
  styleUrls: ['./test-ngif.component.css']
})
export class TestNgifComponent implements OnInit {

  isLogged: boolean;
  user: String;

  constructor() { }

  ngOnInit(): void {
    this.isLogged = true;
    this.user = "Jana";
  }

  changeStatus(): void{
    this.isLogged=!this.isLogged;
  }

}

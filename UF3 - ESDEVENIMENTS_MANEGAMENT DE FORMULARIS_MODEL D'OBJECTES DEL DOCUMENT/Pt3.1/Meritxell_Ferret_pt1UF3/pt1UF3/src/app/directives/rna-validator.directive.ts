import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appRnaValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: RnaValidatorDirective,
    multi: true
  }]
})
export class RnaValidatorDirective {
  RNA_REGEXP = /^[CAGUcagu]+$/;
  constructor() { }

  /**
     * @description
     * Method that performs validation against the provided control.
     *
     * @param {AbstractControl} formFieldToValidate - The control to validate against.
     *
     * @returns A map of validation errors if validation fails,
     * otherwise null.
  */

 validate(formFieldToValidate: AbstractControl): { [key: string]: any } {
  let validInput: boolean = false;

  if (formFieldToValidate && formFieldToValidate.value) {
    validInput = this.RNA_REGEXP.test(formFieldToValidate.value);
  }

  return validInput ? null : { 'isNotCorrect': true };
}

}
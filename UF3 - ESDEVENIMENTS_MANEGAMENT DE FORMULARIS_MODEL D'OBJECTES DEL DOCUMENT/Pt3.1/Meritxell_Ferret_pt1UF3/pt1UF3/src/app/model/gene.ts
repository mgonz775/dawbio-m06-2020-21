export class Gene {

    private id: number;
    private typeMol: String;
    private geneName: String;
    private sequenceGene : String;
    private numExons: number;
    private numIntrons:number;

    constructor(
        id: number, 
        typeMol: String, 
        geneName: String, 
        sequenceGene: String, 
        numExons: number, 
        numIntrons: number
    ) {
        this.id = id
        this.typeMol = typeMol
        this.geneName = geneName
        this.sequenceGene = sequenceGene
        this.numExons = numExons
        this.numIntrons = numIntrons
      }
        

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $geneName
     * @return {String}
     */
	public get $geneName(): String {
		return this.geneName;
	}

    /**
     * Getter $sequenceGene
     * @return {String}
     */
	public get $sequenceGene(): String {
		return this.sequenceGene;
	}

    /**
     * Getter $numExons
     * @return {number}
     */
	public get $numExons(): number {
		return this.numExons;
	}

    /**
     * Getter $numIntrons
     * @return {number}
     */
	public get $numIntrons(): number {
		return this.numIntrons;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $geneName
     * @param {String} value
     */
	public set $geneName(value: String) {
		this.geneName = value;
	}

    /**
     * Setter $sequenceGene
     * @param {String} value
     */
	public set $sequenceGene(value: String) {
		this.sequenceGene = value;
	}

    /**
     * Setter $numExons
     * @param {number} value
     */
	public set $numExons(value: number) {
		this.numExons = value;
	}

    /**
     * Setter $numIntrons
     * @param {number} value
     */
	public set $numIntrons(value: number) {
		this.numIntrons = value;
	}

    /**
     * Getter $typeMol
     * @return {String}
     */
	public get $typeMol(): String {
		return this.typeMol;
	}

    /**
     * Setter $typeMol
     * @param {String} value
     */
	public set $typeMol(value: String) {
		this.typeMol = value;
	}
  
}

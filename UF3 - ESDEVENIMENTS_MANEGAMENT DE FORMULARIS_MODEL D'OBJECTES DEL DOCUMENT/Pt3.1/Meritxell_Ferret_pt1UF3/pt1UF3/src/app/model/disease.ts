export class Disease {

    private id: number;
    private type: String;
    private diseaseName: String;
    private linesInvestigation=Array<string>();

  constructor(id: number, type: String, diseaseName: String, linesInvestigation) {
    this.id = id
    this.type = type
    this.diseaseName = diseaseName
    this.linesInvestigation = linesInvestigation
  }

    /**
     * Getter $id
     * @return {number}
     */
	public get $id(): number {
		return this.id;
	}

    /**
     * Getter $type
     * @return {String}
     */
	public get $type(): String {
		return this.type;
	}

    /**
     * Getter $diseaseName
     * @return {String}
     */
	public get $diseaseName(): String {
		return this.diseaseName;
	}

    /**
     * Setter $id
     * @param {number} value
     */
	public set $id(value: number) {
		this.id = value;
	}

    /**
     * Setter $type
     * @param {String} value
     */
	public set $type(value: String) {
		this.type = value;
	}

    /**
     * Setter $diseaseName
     * @param {String} value
     */
	public set $diseaseName(value: String) {
		this.diseaseName = value;
	}

  /**
     * Setter $linesInvestigation
     * @param {Array} value
     */
	public set $linesInvestigation(value: any) {
		this.diseaseName = value;
	}
  /**
     * Getter $linesInvestigation
     * @return {Array}
     */
	public get $linesInvestigation() :any {
		return this.linesInvestigation;
	}

}

import { Component, OnInit } from '@angular/core';
import { Gene } from '../model/gene';

@Component({
  selector: 'app-genes',
  templateUrl: './genes.component.html',
  styleUrls: ['./genes.component.css']
})
export class GenesComponent implements OnInit {
  objGene: Gene;


  constructor() { }

  ngOnInit(): void {
    this.objGene = new Gene(0, "", "", "", 0, 0);
    
  }

  geneEntry(): void{
    console.log(this.objGene);
  }

}

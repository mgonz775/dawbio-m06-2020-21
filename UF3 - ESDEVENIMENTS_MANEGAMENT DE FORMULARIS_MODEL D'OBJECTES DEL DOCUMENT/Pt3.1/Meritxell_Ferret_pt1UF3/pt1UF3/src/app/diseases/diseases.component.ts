import { Component, OnInit } from '@angular/core';
import { Disease } from '../model/disease';

@Component({
  selector: 'app-diseases',
  templateUrl: './diseases.component.html',
  styleUrls: ['./diseases.component.css']
})
export class DiseasesComponent implements OnInit {
  checkedList = Array<string>();

  lineInvestigation = [
    {line: 'Line01'},
    {line: 'Line02'},
    {line: 'Line03'},
    {line: 'Line04'},
    {line: 'Line05'}
  ];

  objDisease: Disease;



  constructor() { }

  ngOnInit(): void {
    this.objDisease = new Disease(0, "", "", this.checkedList);
    
  }



  diseaseEntry(): void{
    console.log(this.objDisease);
  }

change(e, type){
    if(e.target.checked){
      this.checkedList.push(e.target.name);
    }
    else{
     let updateItem = this.checkedList.find(this.findIndexToUpdate, type.line);

     let index = this.checkedList.indexOf(updateItem);

     this.checkedList.splice(index, 1);
    }

  }

  findIndexToUpdate(type) { 
    return type.line === this;
}
}


import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {Routes, RouterModule} from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GenesComponent } from './genes/genes.component';
import { HomeComponent } from './home/home.component';
import { DnaValidatorDirective } from './directives/dna-validator.directive';
import { RnaValidatorDirective } from './directives/rna-validator.directive';
import { DiseasesComponent } from './diseases/diseases.component';


const appRoutes: Routes = [
  { path: 'app-genes' , component: GenesComponent},
  { path: 'app-home' , component: HomeComponent},
  { path: 'app-diseases' , component: DiseasesComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    GenesComponent,
    HomeComponent,
    DnaValidatorDirective,
    RnaValidatorDirective,
    DiseasesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
